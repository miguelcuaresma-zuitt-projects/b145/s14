console.log("Hello from JS");

// single line comment



/*
	multi-line comment
*/
let name = "miguel";
console.log(name);



let number = 8;
console.log(number);

// Arrays
// are special kind of composite data type that is used to store multiple values.

// lets create a collection of all your subjects in the bootcamp
let bootcampSubjects = ["HTML", "CSS", "Bootstrap", "Javascript"];

// display the output of the array inside the console
console.log(bootcampSubjects);

// rule of thumb when declaring array structures 
// storing multiple data types inside an array is NOT recommended. in a context of programming, this does not make any sense
// an array should be a collection of data that describes a similar/single topic or subject

let details = ["Keanu", "Reeves", 32, true];
console.log(details);

// Objects
// is another special kind of composite data type that is used to mimic or represent a real world object/item
// used to create complex data that contains pieces of information that are relevant to each other.
// every individual piece of code/information is called property of an object.
// lets create an object that describes properties of a cellphone.

// SYNTAX
/*	let/const objectName = {
		key -> value
		propertyA: value
		propertyB: value
	}*/

let cellphone = {
	brand: "Samsung",
	model: "A12",
	color: "Black",
	serialNo: "AS234235632",
	isHomeCredit: true,
	features: ["Calling", "Texting", "Ringing", "5G"]
}

console.log(cellphone)